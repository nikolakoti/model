<?php

require_once __DIR__ . '/vendor/autoload.php';

use Data\Dog;
use Data\Collection;

$dogs = new Collection;

$dogs->addDog(new Dog([
    "breed" => "Beagle",
    "weight" => [
        'male' => '10-11 kg',
        'female' => '9-10 kg'
    ],
    "height" => [
        'male' => '36-41 cm',
        'female' => '33-38 cm'
    ],
    "origin" => "United Kingdom",
    "lifespan" => "12–15 years",
    "temperament" => "Balanced temperament, sociable, gentle, determined, irritating, intelligent"
]))->addDog(new Dog([
    "breed" => "German Shepherd",
    "weight" => [
        'male' => '30–40 kg',
        'female' => '22–33 kg'
    ],
    "height" => [
        'male' => '60–65 cm',
        'female' => '55–60 cm '
    ],
    "origin" => "Germany",
    "lifespan" => "9–13 years",
    "temperament" => "Reliable, intelligent, cautious, brave, obedient, loyal"
]));


//print_r($dogs);

$dogs->dogsList();




