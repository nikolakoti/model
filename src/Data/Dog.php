<?php

namespace Data;

class Dog {

    protected $breed;
    protected $weight;
    protected $height;
    protected $origin;
    protected $lifespan;
    protected $temperament;

    public function __construct(array $properties) {

        if (isset($properties['breed'])) {

            $this->setBreed($properties['breed']);
        }

        if (isset($properties['weight'])) {

            $this->setWeight($properties['weight']);
        }

        if (isset($properties['height'])) {

            $this->setHeight($properties['height']);
        }

        if (isset($properties['origin'])) {

            $this->setOrigin($properties['origin']);
        }

        if (isset($properties['lifespan'])) {

            $this->setLifespan($properties['lifespan']);
        }

        if (isset($properties['temperament'])) {

            $this->setTemperament($properties['temperament']);
        }
    }

    public function getBreed() {
        return $this->breed;
    }

    public function setBreed($breed) {
        $this->breed = $breed;
        return $this;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function setWeight($weight) {
        $this->weight = $weight;
        return $this;
    }

    public function getHeight() {
        return $this->height;
    }

    public function setHeight($height) {
        $this->height = $height;
        return $this;
    }

    public function getOrigin() {
        return $this->origin;
    }

    public function setOrigin($origin) {
        $this->origin = $origin;
        return $this;
    }

    public function getLifespan() {
        return $this->lifespan;
    }

    public function setLifespan($lifespan) {
        $this->lifespan = $lifespan;
        return $this;
    }

    public function getTemperament() {
        return $this->temperament;
    }

    public function setTemperament($temperament) {
        $this->temperament = $temperament;
        return $this;
    }

    public function about() {

        echo $this->getBreed() . " is a breed of dogs originating in " . $this->getOrigin() . "<br><br>";
        echo "<b>Life span:</b> " . $this->getLifespan() . "<br>";
        echo "<b>Temperament:</b> " . $this->getTemperament() . "<br>";
        echo "<b>Height:</b> ";

        foreach ($this->getHeight() as $key => $value) {

            echo $key . ":" . " " . $value . " ";
        }

        echo "." . "<br>";
        echo "<b>Weight:</b> ";

        foreach ($this->getWeight() as $key => $value) {

            echo $key . ":" . " " . $value . " ";
        }
        echo "." . "<br>";
    }

}
