<?php

namespace Data;

use Data\Dog;

class Collection {
    
    
    /*
     * @var Dog[]
     */
    protected $dogs = [];

    public function getDogs(): array {
        return $this->dogs;
    }

    public function setDogs(array $dogs) {

        foreach ($dogs as $dog) {
            if (!($dog instanceof Dog)) {

                die('Members of array $dogs must be instaneces of class Dog');
            }
        }

        $this->dogs = $dogs;
        return $this;
    }

    /**
     * @param Dog $dog
     * @return Collection
     */
    public function addDog(Dog $dog) {

        $this->dogs[] = $dog;

        return $this;
    }

    public function dogsList() {

        echo "<strong>About dogs: </strong>" . "<br><br>";

        foreach ($this->dogs as $dog) {

            echo $dog->about();
            echo "<hr>";
        }
    }

}
